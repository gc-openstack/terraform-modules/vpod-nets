# vpod-nets

Terraform Module: vpod-nets
===========================

This Terraform module creates all of the networks and subnets within a vPOD, along with an Openstack router for external access.

Networks
--------
* RNIC network/subnet (RDMA capable direct 100GbE network)
* Storage network/subnet (RDMA capable direct 100GbE network)
* Control network/subnet (Physical VLAN on 1GbE network)

Variables
=========
Required
--------
* rnic_vlan - VLAN number to be used for RNIC network
* ctrl_vlan - VLAN number to be used for control network
* storage_vlan - VLAN number to be used for storage network
* cidr_hint - number to be used for the third octet of each subnet
* vpod_name - name of the vPOD

Optional
--------
* rnic_physnet - which Openstack 'physical network' will the RNIC (and storage) network be created on (default: physnet2)
* ctrl_physnet - which Openstack 'physical network' will the contro network be created on (default: physnet1)
* dns_base - what DNS base should be used for these networks (default: openstack.example.net)
* external_network_name - which Openstack external network should the router be connected to (default: external)
* storage_net_mtu - what MTU size should be set (handed out by DHCP) on the storage network (default: 1500)

Usage
=====
The module is used per vPOD

    module "networks" {
        source            = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/vpod-nets.git"
        vpod_name         = var.vpod_name
        ctrl_vlan         = var.ctrl_vlan
        rnic_vlan         = var.rnic_vlan
        storage_vlan      = var.storage_vlan
        cidr_hint         = var.cidr_hint
        storage_net_mtu   = var.storage_net_mtu
    }

with example tfvars:

    vpod_name         = "vpod7"
    ctrl_vlan         = 1234
    rnic_vlan         = 2345
    storage_vlan      = 3456
    cidr_hint         = 100
    ipum_csv          = "rnic_rack8.csv"
    vpod_floatingip   = "1.2.3.4"
    poplaraz          = "rack8-AZ"
    assigned_ipums    = ["rack8-ipum1", "rack8-ipum2", "rack8-ipum3", "rack8-ipum4", "rack8-ipum5", "rack8-ipum6", "rack8-ipum7", "rack8-ipum8", "rack8-ipum9", "rack8-ipum10", "rack8-ipum11", "rack8-ipum12", "rack8-ipum13", "rack8-ipum14", "rack8-ipum15", "rack8-ipum16" ]
    flavor            = "r6525.full"

Development and Feedback
========================
The development of this tooling is ongoing, and any feedback is appreciated.  If you find any errors or bugs, please create an issue on the appropriate repository, or contact your Graphcore representative.