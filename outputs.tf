output "rnic_network_id" {
  value = openstack_networking_network_v2.rnic.id
}

output "rnic_subnet_id" {
  value = openstack_networking_subnet_v2.rnic.id
}

output "ctrl_network_id" {
  value = openstack_networking_network_v2.ctrl.id
}

output "ctrl_subnet_id" {
  value = openstack_networking_subnet_v2.ctrl.id
}

output "storage_network_id" {
  value = openstack_networking_network_v2.storage.id
}

output "storage_subnet_id" {
  value = openstack_networking_subnet_v2.storage.id
}

output "vpod_internal_sg" {
  value = openstack_networking_secgroup_v2.vpod.id
}