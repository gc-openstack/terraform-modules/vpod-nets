#
# Required
#
variable "rnic_vlan" {
  type = number
}

variable "ctrl_vlan" {
  type = number
}

variable "storage_vlan" {
  type = number
}

variable "cidr_hint" {
  type = number
}

variable "vpod_name" {
  type = string
}

#
# Optional
#
variable "rnic_physnet" {
  default = "physnet2"
}

variable "ctrl_physnet" {
  default = "physnet1"
}

variable "dns_base" {
  type    = string
  default = "openstack.example.net."
}

variable "external_network_name" {
  default = "external"
}

variable "storage_net_mtu" {
  type = number
}
