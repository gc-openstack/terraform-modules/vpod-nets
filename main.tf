terraform {
  required_version = ">= 1.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.35"
    }
  }
}

#
# Create rnic network and subnet
#

resource "openstack_networking_network_v2" "rnic" {
  name       = "${var.vpod_name}-rnic"
  mtu        = 1500
  dns_domain = "${var.vpod_name}.${var.dns_base}"
  segments {
    physical_network = var.rnic_physnet
    segmentation_id  = var.rnic_vlan
    network_type     = "vlan"
  }
  # avoid connection tracking in ovn
  port_security_enabled = false
}

resource "openstack_networking_subnet_v2" "rnic" {
  name       = "${var.vpod_name}-rnic"
  network_id = openstack_networking_network_v2.rnic.id
  cidr       = "10.5.0.0/16"
  ip_version = 4
  no_gateway = true
  allocation_pool {
    start = "10.5.${var.cidr_hint}.100"
    end   = "10.5.${var.cidr_hint}.199"
  }
}


#
# Create rnic network and subnet
#

resource "openstack_networking_network_v2" "storage" {
  name       = "${var.vpod_name}-storage"
  mtu        = var.storage_net_mtu
  dns_domain = "${var.vpod_name}-storage.${var.dns_base}"
  segments {
    physical_network = var.rnic_physnet
    segmentation_id  = var.storage_vlan
    network_type     = "vlan"
  }
  # avoid connection tracking in ovn
  port_security_enabled = false
}

resource "openstack_networking_subnet_v2" "storage" {
  name       = "${var.vpod_name}-storage"
  network_id = openstack_networking_network_v2.storage.id
  cidr       = "10.12.${var.cidr_hint}.0/24"
  ip_version = 4
  no_gateway = true
  allocation_pool {
    start = "10.12.${var.cidr_hint}.100"
    end   = "10.12.${var.cidr_hint}.199"
  }
}

#
# Create ctrl network and subnet
#

resource "openstack_networking_network_v2" "ctrl" {
  name           = "${var.vpod_name}-ctrl"
  mtu            = 1500
  admin_state_up = "true"
  segments {
    physical_network = var.ctrl_physnet
    segmentation_id  = var.ctrl_vlan
    network_type     = "vlan"
  }
}

resource "openstack_networking_subnet_v2" "ctrl" {
  name       = "${var.vpod_name}-ctrl"
  network_id = openstack_networking_network_v2.ctrl.id
  cidr       = "10.3.0.0/16"
  allocation_pool {
    start = "10.3.${var.cidr_hint}.100"
    end   = "10.3.${var.cidr_hint}.199"
  }
}

data "openstack_networking_network_v2" "external" {
  name = var.external_network_name
}

resource "openstack_networking_router_v2" "ctrl" {
  name                = "${var.vpod_name}-ctrl"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.external.id
}

resource "openstack_networking_router_interface_v2" "ctrl" {
  router_id = openstack_networking_router_v2.ctrl.id
  subnet_id = openstack_networking_subnet_v2.ctrl.id
}

resource "openstack_networking_secgroup_v2" "vpod" {
  name        = "${var.vpod_name}-vpod-sg"
  description = "Security group to allow trusted access between Poplar hosts (and also bastion if present)"
}

resource "openstack_networking_secgroup_rule_v2" "ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  remote_group_id   = openstack_networking_secgroup_v2.vpod.id
  security_group_id = openstack_networking_secgroup_v2.vpod.id
}
